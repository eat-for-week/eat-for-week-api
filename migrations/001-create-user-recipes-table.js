export const up = async sql => {
    await sql`
      CREATE TABLE user_recipes (
        id uuid DEFAULT gen_random_uuid() NOT NULL,
        user_id uuid NOT NULL,
        recipe_id uuid NOT NULL,
        date_created timestamp without time zone DEFAULT CURRENT_TIMESTAMP
      );
    `;
};

export const down = async sql => {
    await sql`DROP TABLE user_recipes`;
};
