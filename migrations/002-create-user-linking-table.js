export const up = async sql => {
    await sql`
      CREATE TABLE user_linking (
        id uuid DEFAULT gen_random_uuid() NOT NULL,
        user_id1 character(36) NOT NULL,
        user_id2 character(36) NOT NULL,
        date_created timestamp without time zone DEFAULT CURRENT_TIMESTAMP
      );
    `;
};

export const down = async sql => {
    await sql`DROP TABLE user_linking`;
};
