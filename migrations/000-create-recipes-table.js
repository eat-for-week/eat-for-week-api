export const up = async sql => {
    await sql`
      CREATE TABLE recipes (
        id uuid DEFAULT gen_random_uuid() NOT NULL,
        author text,
        canonical_url text,
        category text,
        cook_time smallint,
        cuisine text,
        description text,
        host text,
        image text,
        ingredients text[],
        instructions_list text[],
        language text,
        prep_time smallint,
        ratings numeric,
        site_name text,
        title text,
        total_time smallint,
        yields text,
        image_local text
      );
    `;
};

export const down = async sql => {
    await sql`DROP TABLE recipes`;
};
