export const up = async sql => {
    await sql`
      CREATE TABLE user_recipe_ingredients_edits (
        id UUID DEFAULT gen_random_uuid() NOT NULL,
        user_recipe_id UUID REFERENCES user_recipes(id) UNIQUE,
        ingredients TEXT[] NOT NULL,
        edited_by UUID NOT NULL,
        edited_on TIMESTAMP WITHOUT TIME ZONE
      );
    `;

    await sql`
      CREATE TABLE user_recipe_instructions_list_edits (
        id UUID DEFAULT gen_random_uuid() NOT NULL,
        user_recipe_id UUID REFERENCES user_recipes(id) UNIQUE,
        instructions_list TEXT[] NOT NULL,
        edited_by UUID NOT NULL,
        edited_on TIMESTAMP WITHOUT TIME ZONE
      );
    `;
};

export const down = async sql => {
    await sql`DROP TABLE user_recipe_ingredients_edits`;
    await sql`DROP TABLE user_recipe_instructions_list_edits`;
};
