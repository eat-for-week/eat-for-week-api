import jsConfig         from '@eslint/js';

import stylisticPlugin  from '@stylistic/eslint-plugin';

import unicornPlugin    from 'eslint-plugin-unicorn';
import globals          from 'globals';


const rules = {
    ...jsConfig.configs.all.rules,
    ...stylisticPlugin.configs['all-flat'].rules,
    ...unicornPlugin.configs.all.rules,

    '@stylistic/array-element-newline': ['error', 'consistent'],
    '@stylistic/arrow-parens': ['error', 'as-needed'],
    '@stylistic/dot-location': ['error', 'property'],
    '@stylistic/function-call-argument-newline': ['error', 'consistent'],
    '@stylistic/function-paren-newline': 'off',
    '@stylistic/key-spacing': 'off',
    '@stylistic/multiline-ternary': 'off',
    '@stylistic/newline-per-chained-call': 'off',
    '@stylistic/no-extra-parens': 'off',
    '@stylistic/no-multi-spaces': ['error', {exceptions: {VariableDeclarator: true, ImportDeclaration: true}}],
    '@stylistic/object-property-newline': ['error', {allowAllPropertiesOnSameLine: true}],
    '@stylistic/padded-blocks': 'off',
    '@stylistic/quotes': ['error', 'single'],
    '@stylistic/quote-props': ['error', 'as-needed'],

    camelcase: 'off',
    'capitalized-comments': 'off',
    'consistent-return': 'off',
    'id-length': 'off',
    'line-comment-position': 'off',
    'max-statements': 'off',
    'max-lines-per-function': 'off',
    'multiline-comment-style': 'off',
    'new-cap': 'off',
    'no-console': 'off',
    'no-inline-comments': 'off',
    'no-magic-numbers': 'off',
    'no-nested-ternary': 'off',
    'no-plusplus': 'off',
    'no-promise-executor-return': 'off',
    'no-ternary': 'off',
    'no-undefined': 'off',
    'one-var': 'off',
    'sort-imports': 'off',
    'sort-keys': 'off',

    // Reports false positives - Refer to https://github.com/sindresorhus/eslint-plugin-unicorn/issues/1193
    'unicorn/no-array-callback-reference': 'off',

    // Reports false positives - Refer to https://github.com/sindresorhus/eslint-plugin-unicorn/issues/1394
    'unicorn/no-array-method-this-argument': 'off',

    'unicorn/no-array-reduce': 'off',
    'unicorn/no-nested-ternary': 'off',
    'unicorn/no-null': 'off'
};


export default [
    {
        files: ['**/*.js'],

        languageOptions: {
            globals: globals.node
        },

        linterOptions: {
            noInlineConfig: true
        },

        plugins: {
            '@stylistic': stylisticPlugin,
            unicorn: unicornPlugin
        },

        rules
    }
];
