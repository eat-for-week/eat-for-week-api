import crypto   from 'node:crypto';
import fs       from 'node:fs';
import https    from 'node:https';
import path     from 'node:path';


export const getImageNameHash = imageUrl => {
    const filename = crypto.createHash('md5').update(imageUrl).digest('hex');
    const filetype = path.basename(new URL(imageUrl).pathname).match(/.+?\.(?<filetype>.+)$/u)?.groups.filetype;
    if (!filetype) {
        return filename;
    }

    return `${filename}.${filetype}`;
};

export const getImageBinaryData = imageUrl => new Promise((resolve, reject) => {
    const request = https.request(imageUrl, response => {
        if (response.statusCode >= 300 && response.statusCode < 400 && response.headers.location) {
            return getImageBinaryData(response.headers.location).then(resolve).catch(reject);
        }
        if (!response.headers['content-type'].startsWith('image/')) {
            return reject(new Error('Image not resolved'));
        }

        let imageBinaryData = '';
        response.setEncoding('binary');
        response.on('data', chunk => (imageBinaryData += chunk));
        response.on('end', () => resolve(imageBinaryData));
    });
    request.on('error', error => reject(error));
    request.end();
});

export const backupImage = async image => {
    if (!image) {
        return null;
    }

    const imageNameHash = getImageNameHash(image);

    let imageBinaryData = null;
    try {
        imageBinaryData = await getImageBinaryData(image);
    } catch (error) {
        console.error(`Error downloading image binary for ${image}`);
        console.error(error);
        return null;
    }

    try {
        await fs.promises.writeFile(`${process.env.IMAGE_DIR}/${imageNameHash}`, imageBinaryData, {encoding: 'binary'});
    } catch (error) {
        console.error(`Error writing image file for ${image}`);
        console.error(error);
        return null;
    }

    return imageNameHash;
};
