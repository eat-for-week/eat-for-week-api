import {spawn}          from 'node:child_process';
import {fileURLToPath}  from 'node:url';

import cors             from '@fastify/cors';
import formDataPlugin   from '@fastify/formbody';

import Fastify          from 'fastify';
import postgres         from 'postgres';

import supertokens      from 'supertokens-node/index.js';
import {
    errorHandler as supertokensFastifyErrorHandler,
    plugin as supertokensFastifyPlugin
}                       from 'supertokens-node/framework/fastify/index.js';
import Session          from 'supertokens-node/recipe/session/index.js';
import {verifySession}  from 'supertokens-node/recipe/session/framework/fastify/index.js';
import EmailPassword    from 'supertokens-node/recipe/emailpassword/index.js';

import {backupImage}    from './images.js';


const fastify = Fastify();
fastify.setErrorHandler(supertokensFastifyErrorHandler());

if (!process.env.IMAGE_DIR) {
    throw new Error('IMAGE_DIR env variable is required');
}

const sql = postgres();
try {
    await sql`SELECT 1`;
} catch (error) {
    console.error('Postgres connection failed!\nAre PGUSER, PGPASSWORD, PGDATABASE, and PGHOST set?');
    throw error;
}

const port = process.env.PORT || 3000;

supertokens.init({
    framework: 'fastify',
    supertokens: {
        connectionURI: 'http://192.168.1.123:3567'
    },
    appInfo: {
        appName: 'Eat for Week',
        apiDomain: process.env.NODE_ENV === 'production' ? 'https://eatforweek-api.duckdns.org' : 'http://127.0.0.1:3000',
        websiteDomain: process.env.NODE_ENV === 'production' ? 'https://eatforweek.duckdns.org' : 'http://127.0.0.1:8080'
    },
    recipeList: [
        EmailPassword.init(),
        Session.init()
    ]
});

fastify.register(cors, {
    origin: process.env.NODE_ENV === 'production' ? 'https://eatforweek.duckdns.org' : ['http://localhost:8008', 'http://127.0.0.1:8080'],
    allowedHeaders: ['Content-Type', ...supertokens.getAllCORSHeaders()],
    credentials: true
});

await fastify.register(formDataPlugin);
await fastify.register(supertokensFastifyPlugin);

fastify.listen({host: '0.0.0.0', port}, (error, address) => {
    if (error) {
        fastify.log.error(error);
        throw error;
    }

    console.log(`Eat For Week API is now listening on ${address}`);
});

const pythonRecipeScraperFile = `${fileURLToPath(new URL('.', import.meta.url))}/scrape-recipe.py`;
fastify.get('/recipe/:url', {preHandler: verifySession()}, (request, reply) => {
    const scrapePython = spawn('python', [pythonRecipeScraperFile, decodeURIComponent(request.params.url)]);

    let data = '';
    let error = '';
    scrapePython.stdout.on('data', stdoutData => (data += stdoutData));
    scrapePython.stderr.on('data', stderrData => (error += stderrData));
    scrapePython.on('close', () => {
        if (error) {
            if (error.includes('WebsiteNotImplementedError')) {
                reply.status(501).send(new Error('Website not supported'));
            } else {
                reply.status(500).send(new Error(error));
            }
            return;
        }
        const recipeJson = JSON.parse(data);
        reply.send(recipeJson);
    });
});

const getUserRecipes = (userId, sqlTransaction) => sqlTransaction`
  WITH user_ids AS (
    SELECT ${userId} AS user_id
    UNION
    SELECT ul.user_id1 AS user_id
    FROM user_linking ul
    WHERE ul.user_id2 = ${userId}
    UNION
    SELECT ul.user_id2 AS user_id
    FROM user_linking ul
    WHERE ul.user_id1 = ${userId}
  ) SELECT
   r.*,
   ur.id AS id,
   r.id AS recipe_id,
   ur.user_id AS user_id,
   ur.date_created AS date_created,
   COALESCE(urie.ingredients, r.ingredients) AS ingredients,
   COALESCE(urile.instructions_list, r.instructions_list) AS instructions_list,
   urie.edited_by AS ingredients_edited_by,
   urie.edited_on AS ingredients_edited_on,
   urile.edited_by AS instructions_list_edited_by,
   urile.edited_on AS instructions_list_edited_on
  FROM recipes r
  JOIN user_recipes ur ON ur.recipe_id = r.id
  JOIN user_ids ui ON ur.user_id::text = ui.user_id
  LEFT JOIN user_recipe_ingredients_edits urie ON urie.user_recipe_id = ur.id
  LEFT JOIN user_recipe_instructions_list_edits urile ON urile.user_recipe_id = ur.id
  ORDER BY date_created DESC
`;
fastify.get('/recipes', {preHandler: verifySession()}, async request => await getUserRecipes(request.session.getUserId(), sql));

const getUserLinking = (userId, sqlTransaction) => sqlTransaction`
  SELECT ul.id AS id, ul.user_id2 AS user_id, epu.email AS email, ul.date_created AS date_created
  FROM user_linking ul
  JOIN supertokens.emailpassword_users epu ON epu.user_id = ul.user_id2
  WHERE ul.user_id1 = ${userId}
  UNION
  SELECT ul.id AS id, ul.user_id1 AS user_id, epu.email AS email, ul.date_created AS date_created
  FROM user_linking ul
  JOIN supertokens.emailpassword_users epu ON epu.user_id = ul.user_id1
  WHERE ul.user_id2 = ${userId}
  ORDER BY date_created DESC
`;
fastify.get('/linking', {preHandler: verifySession()}, async request => await getUserLinking(request.session.getUserId(), sql));


const getUserLinkingRollup = async (currentUserId, sqlTransaction) => {
    const [recipes, user_linking] = await Promise.all([
        getUserRecipes(currentUserId, sqlTransaction),
        getUserLinking(currentUserId, sqlTransaction)
    ]);
    return {recipes, user_linking};
};

fastify.put('/edit/:user_recipe_id', {preHandler: verifySession()}, async request => {
    if (!(request.body.ingredients || request.body.instructions_list)) {
        const error = new Error('Missing \'ingredients\' or \'instructions_list\' param');
        error.statusCode = 400;
        throw error;
    }

    const currentUserId = request.session.getUserId();
    const [[existingRecipe], userLinking] = await Promise.all([
        sql`SELECT * FROM user_recipes WHERE id = ${request.params.user_recipe_id}`,
        getUserLinking(currentUserId, sql)
    ]);
    if (!existingRecipe) {
        const error = new Error('Recipe not found');
        error.statusCode = 404;
        throw error;
    }

    const userIdsOfEditableRecipes = new Set([...userLinking.map(ul => ul.user_id), currentUserId]);
    if (!userIdsOfEditableRecipes.has(existingRecipe.user_id)) {
        const error = new Error('Recipe not editable');
        error.statusCode = 403;
        throw error;
    }

    const [recipe] = await sql.begin(async sqlTransaction => {
        const upsertIngredientsEditPromise = request.body.ingredients && sqlTransaction`
            INSERT INTO user_recipe_ingredients_edits (user_recipe_id, ingredients, edited_by, edited_on)
            VALUES (${request.params.user_recipe_id}, ${request.body.ingredients}, ${currentUserId}, ${sql`now()`})
            ON CONFLICT (user_recipe_id) DO UPDATE
            SET
                ingredients = ${request.body.ingredients},
                edited_by = ${currentUserId},
                edited_on = ${sql`now()`}
        `;
        const upsertInstructionsListEditPromise = request.body.instructions_list && sqlTransaction`
            INSERT INTO user_recipe_instructions_list_edits (user_recipe_id, instructions_list, edited_by, edited_on)
            VALUES (${request.params.user_recipe_id}, ${request.body.instructions_list}, ${currentUserId}, ${sql`now()`})
            ON CONFLICT (user_recipe_id) DO UPDATE
            SET
                instructions_list = ${request.body.instructions_list},
                edited_by = ${currentUserId},
                edited_on = ${sql`now()`}
        `;

        await Promise.all([upsertIngredientsEditPromise, upsertInstructionsListEditPromise]);

        return await sqlTransaction`
            SELECT
                r.*,
                r.id AS recipe_id,
                ur.id AS id,
                ur.user_id AS user_id,
                ur.date_created AS date_created,
                COALESCE(urie.ingredients, r.ingredients) AS ingredients,
                COALESCE(urile.instructions_list, r.instructions_list) AS instructions_list,
                urie.edited_by AS ingredients_edited_by,
                urie.edited_on AS ingredients_edited_on,
                urile.edited_by AS instructions_list_edited_by,
                urile.edited_on AS instructions_list_edited_on
            FROM recipes r
            JOIN user_recipes ur ON ur.id = ${request.params.user_recipe_id} AND ur.recipe_id = r.id
            LEFT JOIN user_recipe_ingredients_edits urie ON urie.user_recipe_id = ur.id
            LEFT JOIN user_recipe_instructions_list_edits urile ON urile.user_recipe_id = ur.id
        `;
    });
    return recipe;
});

fastify.delete('/linking/:user_linking_id', {preHandler: verifySession()}, async request => await sql.begin(async sqlTransaction => {
    await sqlTransaction`
      DELETE FROM user_linking ul WHERE ul.id = ${request.params.user_linking_id}
    `;

    return await getUserLinkingRollup(request.session.getUserId(), sqlTransaction);
}));
fastify.post('/linking/:user_linking_email', {preHandler: verifySession()}, async request => await sql.begin(async sqlTransaction => {
    const currentUserId = request.session.getUserId();
    const email = decodeURIComponent(request.params.user_linking_email);

    const [linkedUser] = await supertokens.listUsersByAccountInfo('public', {email});
    if (!linkedUser) {
        const error = new Error('User not found');
        error.statusCode = 404;
        throw error;
    }

    await sqlTransaction`
      INSERT INTO user_linking (user_id1, user_id2)
      SELECT ${currentUserId}, ${linkedUser.id}
      WHERE NOT EXISTS (
        SELECT 1
        FROM user_linking ul
        WHERE
          (ul.user_id1 = ${currentUserId} AND ul.user_id2 = ${linkedUser.id})
          OR
          (ul.user_id2 = ${currentUserId} AND ul.user_id1 = ${linkedUser.id})
      )
    `;

    return await getUserLinkingRollup(currentUserId, sqlTransaction);
}));

fastify.post('/recipe', {preHandler: verifySession()}, async request => await sql.begin(async sqlTransaction => {
    let recipeRows = await sqlTransaction`SELECT * FROM recipes WHERE canonical_url = ${request.body.canonical_url}`;
    if (recipeRows.length === 0) {
        // Download image and save locally
        const localImageName = await backupImage(request.body.image);

        // Insert recipe into table
        recipeRows = await sqlTransaction`
            INSERT INTO recipes
                (author, canonical_url, category, cook_time, cuisine, description, host, image, image_local, ingredients, instructions_list, language, prep_time, ratings, site_name, title, total_time, yields)
            VALUES
                (${request.body.author}, ${request.body.canonical_url}, ${request.body.category}, ${request.body.cook_time || null}, ${request.body.cuisine || null}, ${request.body.description || null}, ${request.body.host}, ${request.body.image}, ${localImageName}, ${request.body.ingredients}, ${request.body.instructions_list}, ${request.body.language}, ${request.body.prep_time || null}, ${request.body.ratings}, ${request.body.site_name}, ${request.body.title}, ${request.body.total_time}, ${request.body.yields})
            RETURNING *
        `;
    }

    await sqlTransaction`
        INSERT INTO user_recipes
            (user_id, recipe_id)
        VALUES
            (${request.session.getUserId()}, ${recipeRows[0].id})
    `;

    return recipeRows[0];
}));
