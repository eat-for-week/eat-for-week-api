import json
import sys
from recipe_scrapers import scrape_me

scraper = scrape_me(sys.argv[1], wild_mode=True)

print(json.dumps(scraper.to_json()))
