import postgres         from 'postgres';

import {backupImage}    from './images.js';


const sql = postgres();

const recipeImages = await sql`SELECT id, image FROM recipes`;
const recipeIdToLocalImageNames = await Promise.all(recipeImages.map(
    ({id: recipeId, image}) => new Promise(resolve => backupImage(image)
        .then(localImageName => resolve({image, localImageName, recipeId}))
        .catch(error => resolve({error}))
    )
));

await sql.begin(async sqlTransaction => await Promise.all(recipeIdToLocalImageNames.map(async ({error, image, localImageName, recipeId}) => {
    if (error) {
        console.log(`Recipe could not back up image: [Recipe ID: ${recipeId}] [Image: ${image}]`, error);
        return true;
    }
    try {
        await sqlTransaction`
            UPDATE recipes SET image_local = ${localImageName} WHERE id = ${recipeId}
        `;
    } catch (sqlTransactionError) {
        console.log(`Recipe could not update image: [Recipe ID: ${recipeId}] [Image: ${image}]`, sqlTransactionError);
    }

    return true;
})));
