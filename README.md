# eat-for-week-api

## Local Dev

### Prerequisites

#### Installed Technologies
- [Node.js 20](https://nodejs.org/en)
- [pnpm](https://pnpm.io/)
- [python3](https://www.python.org/)
- [podman](https://podman.io/) or [docker](https://www.docker.com/)

<br>

#### Running Containers
##### Examples using [podman](https://podman.io/)
- [Postgres](https://hub.docker.com/_/postgres)
  ```sh
  podman run --detach --network=host --restart=unless-stopped --name=eat-for-week-postgres --env POSTGRES_PASSWORD=<CHOSEN_PASSWORD_HERE> --env POSTGRES_DB=eatforweek docker.io/library/postgres:alpine
  ```
- [Supertokens Core](https://hub.docker.com/r/supertokens/supertokens-postgresql)
  ```sh
  podman run --detach --network=host --restart=unless-stopped --name=eat-for-week-supertokens --env POSTGRESQL_CONNECTION_URI=postgresql://postgres:<CHOSEN_PASSWORD_HERE>@127.0.0.1:5432/eatforweek --env POSTGRESQL_TABLE_SCHEMA=supertokens docker.io/supertokens/supertokens-postgresql
  ```
<br>

### Project setup
#### Set Required Environment Variables on Shell
```sh
export IMAGE_DIR=/home/eat-for-week-images PORT=3000 PGUSER=postgres PGPASSWORD='<CHOSEN_PASSWORD_HERE>' PGDATABASE=eatforweek PGPORT=5432 PGHOST=127.0.0.1
```

#### Install Dependencies
```sh
pnpm install
pip install --no-cache-dir -r requirements.txt
```

#### Run Migrations
```sh
pnpm ley up
```

<br>

### Start Application In Watch Mode
```sh
node --watch src/index.js
```
